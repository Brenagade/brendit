'use strict'

const url = require('url')
const WebSocket = require('ws')

const wss = new WebSocket.Server({ port: 13001 })

wss.on('open', function open() {
  console.log('websocket opened')
  wss.send(Date.now())
})

wss.on('connection', function connection(ws, req) {
  const location = url.parse(req.url, true)

  console.log('wss.connection().location', location)
  // You might use location.query.access_token to authenticate or share sessions
  // or req.headers.cookie (see http://stackoverflow.com/a/16395220/151312)
  // You may want to provide topics or queues. The location.path can be used.
  // (see https://stackoverflow.com/questions/22429744/how-to-setup-route-for-websocket-server-in-express)
  console.log(`Client wants to use destination: ${location.path}`)

  ws.on('message', function incoming(message) {
    console.log('received: %s', message)
    ws.send('Server received from client: ' + message)
  })

  ws.on('close', function close() {
    console.log('websocket disconnected')
  })
})

// 'use strict';
//
// const url = require('url')
// const https = require('https');
// const fs = require('fs');
//
// const WebSocket = require('ws');
//
// const server = https.createServer({
//   cert: fs.readFileSync('/brendit/conf/public-cert.pem'),
//   key: fs.readFileSync('/brendit/conf/private-key.pem')
// });
//
// const wss = new WebSocket.Server({ server });
//
// wss.on('open', function open () {
//   console.log('websocket opened');
//   wss.send(Date.now());
// });
//
// wss.on('connection', function connection (ws, req) {
//   const location = url.parse(req.url, true);
//
//   console.log('wss.connection().location', location);
//   // You might use location.query.access_token to authenticate or share sessions
//   // or req.headers.cookie (see http://stackoverflow.com/a/16395220/151312)
//   // You may want to provide topics or queues. The location.path can be used.
//   // (see https://stackoverflow.com/questions/22429744/how-to-setup-route-for-websocket-server-in-express)
//   console.log(`Client wants to use destination: ${location.path}`);
//
//   ws.on('message', function incoming (message) {
//     console.log('received: %s', message);
//     ws.send('Server received from client: ' + message);
//   })
//
//   ws.on('close', function close () {
//     console.log('websocket disconnected');
//   })
// })
//
// server.listen(function listening() {
//   //
//   // If the `rejectUnauthorized` option is not `false`, the server certificate
//   // is verified against a list of well-known CAs. An 'error' event is emitted
//   // if verification fails.
//   //
//   // The certificate used in this example is self-signed so `rejectUnauthorized`
//   // is set to `false`.
//   //
//
//   const ws = new WebSocket(`wss://localhost:${server.address().port}/messaging/echo`, {
//     rejectUnauthorized: false
//   });
//
//   ws.on('open', function open() {
//     ws.send('All glory to WebSockets!');
//   });
// });

// 'use strict';
//
// const https = require('https');
// const fs = require('fs');
//
// const WebSocket = require('..');
//
// const server = https.createServer({
//   cert: fs.readFileSync('../test/fixtures/certificate.pem'),
//   key: fs.readFileSync('../test/fixtures/key.pem')
// });
//
// const wss = new WebSocket.Server({ server });
//
// wss.on('connection', function connection(ws) {
//   ws.on('message', function message(msg) {
//     console.log(msg);
//   });
// });
//
// server.listen(function listening() {
//   //
//   // If the `rejectUnauthorized` option is not `false`, the server certificate
//   // is verified against a list of well-known CAs. An 'error' event is emitted
//   // if verification fails.
//   //
//   // The certificate used in this example is self-signed so `rejectUnauthorized`
//   // is set to `false`.
//   //
//   const ws = new WebSocket(`wss://localhost:${server.address().port}`, {
//     rejectUnauthorized: false
//   });
//
//   ws.on('open', function open() {
//     ws.send('All glory to WebSockets!');
//   });
// });
