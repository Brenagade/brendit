//Connect to Mongo database
const mongoose = require('mongoose')
mongoose.Promise = global.Promise

//your local database url
//27017 is the default mongoDB port
// const uri = 'mongodb://root:example@184.151.127.26:27017/brendit'
const uri = 'mongodb://192.168.0.151:27017/brendit'
mongoose.connect(uri).then(
  () => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */

    console.log('Connected to Mongo')
  },
  err => {
    /** handle initial connection error */

    console.log('error connecting to Mongo: ')
    console.log(err)
  }
)

module.exports = mongoose.connection
