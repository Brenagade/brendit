const TYPED_SNACKBAR_ADD_MESSAGE = 'TYPED_SNACKBAR_ADD_MESSAGE'
const TYPED_SNACKBAR_GET_MESSAGE = 'TYPED_SNACKBAR_GET_MESSAGE'
const TYPED_SNACKBAR_CLOSE_MESSAGE = 'TYPED_SNACKBAR_CLOSE_MESSAGE'

const typedSnackbarActionTypes = {
  TYPED_SNACKBAR_ADD_MESSAGE,
  TYPED_SNACKBAR_GET_MESSAGE,
  TYPED_SNACKBAR_CLOSE_MESSAGE,
}

const successSnackbar = msg => {
  return {
    type: TYPED_SNACKBAR_ADD_MESSAGE,
    message: msg,
    variant: 'success',
  }
}

const infoSnackbar = msg => {
  return {
    type: TYPED_SNACKBAR_ADD_MESSAGE,
    message: msg,
    variant: 'info',
  }
}

const warningSnackbar = msg => {
  return {
    type: TYPED_SNACKBAR_ADD_MESSAGE,
    message: msg,
    variant: 'warning',
  }
}

const errorSnackbar = msg => {
  return {
    type: TYPED_SNACKBAR_ADD_MESSAGE,
    message: msg,
    variant: 'error',
  }
}

const getMessage = () => {
  return {
    type: TYPED_SNACKBAR_GET_MESSAGE,
  }
}

const closeMessage = () => {
  return {
    type: TYPED_SNACKBAR_CLOSE_MESSAGE,
  }
}

export {
  successSnackbar,
  infoSnackbar,
  warningSnackbar,
  errorSnackbar,
  getMessage,
  closeMessage,
  typedSnackbarActionTypes,
}
