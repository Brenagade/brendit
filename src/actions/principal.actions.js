const SIGN_IN_SHOW = 'SIGN_IN_SHOW'
const SIGN_IN_HIDE = 'SIGN_IN_HIDE'
const SET_CREDENTIALS = 'SET_CREDENTIALS'
const SET_API_KEY = 'SET_API_KEY'

const principalActionTypes = {
  SIGN_IN_SHOW,
  SIGN_IN_HIDE,
  SET_CREDENTIALS,
  SET_API_KEY: SET_API_KEY,
}

const showSignInForm = () => {
  return {
    type: SIGN_IN_SHOW,
  }
}

const hideSignInForm = () => {
  return {
    type: SIGN_IN_HIDE,
  }
}

const setCredentials = credentials => {
  return {
    type: SET_CREDENTIALS,
    payload: credentials,
  }
}

const setAPIKey = key => {
  return {
    type: SET_API_KEY,
    payload: key,
  }
}

export {
  showSignInForm,
  hideSignInForm,
  setCredentials,
  setAPIKey,
  principalActionTypes,
}
