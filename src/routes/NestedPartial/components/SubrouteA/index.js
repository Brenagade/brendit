import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import NotFound from '../../../NotFound'

const styles = {
  container: {
    flexGrow: 1,
    align: 'center',
  },
}

class SubrouteA extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
  }

  render() {
    const { isExact } = this.props.match
    if (isExact) {
      return (
        <div style={styles.container}>
          <br />
          <Typography variant="h5" gutterBottom align="center">
            Page A
          </Typography>
        </div>
      )
    } else {
      return <NotFound />
    }
  }
}

export default SubrouteA
