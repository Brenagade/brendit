import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link, Route, Switch } from 'react-router-dom'
import classNames from 'classnames'
// Material-UI Components
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
// Icons
import FlightTakeoffIcon from '@material-ui/icons/FlightTakeoff'
import VideocamOff from '@material-ui/icons/VideocamOff'
// Colors
import blue from '@material-ui/core/colors/blue'
import green from '@material-ui/core/colors/green'
import red from '@material-ui/core/colors/red'
// Components
import SubrouteA from './components/SubrouteA'
import SubrouteB from './components/SubrouteB'
import NotFound from '../NotFound'

const styles = theme => ({
  iconStyles: {
    marginRight: '24px',
  },
  container: {
    display: 'flex',
    flexGrow: 1,
    alignItems: 'center',
  },
  icon: {
    margin: theme.spacing.unit,
  },
  iconBlue: {
    fill: blue[500],
  },
  iconRed: {
    fill: red[500],
  },
  iconHover: {
    fill: red[500],
    '&:hover': {
      fill: green[200],
    },
  },
})

class NestedPartial extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
  }

  renderRoute(url, isExact) {
    // if exact we are on NestedPartial, otherwise try to render one of the sub-routes
    if (!isExact) {
      return (
        <Switch>
          <Route path={`${url}/A`} component={SubrouteA} />
          <Route path={`${url}/B`} component={SubrouteB} />
          <Route component={NotFound} />
        </Switch>
      )
    }
  }

  render() {
    const { url, isExact } = this.props.match
    const { classes } = this.props
    return (
      <div className={[classes.container, 'column-container']}>
        <Typography varian="title" gutterBottom align="center">
          Route With Sub-Routes
        </Typography>
        <Typography type="subheading" gutterBottom align="center">
          Sub-route is rendered as a Partial on the same page as the Parent
        </Typography>
        <br />
        <List>
          <Divider />
          <ListItem component={Link} to={`${url}/A`}>
            <ListItemIcon
              className={classNames(classes.icon, classes.iconBlue)}>
              <FlightTakeoffIcon />
            </ListItemIcon>
            <ListItemText primary="Sub-route A" />
          </ListItem>
          <Divider />
          <ListItem component={Link} to={`${url}/B`}>
            <ListItemIcon className={classNames(classes.icon, classes.iconRed)}>
              <VideocamOff />
            </ListItemIcon>
            <ListItemText primary="Sub-route B" />
          </ListItem>
          <Divider />
        </List>
        {this.renderRoute(url, isExact)}
      </div>
    )
  }
}

export default withStyles(styles)(NestedPartial)
