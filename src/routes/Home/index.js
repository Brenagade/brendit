import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'

import reactLogo from '../../images/react.svg'
import reduxLogo from '../../images/redux.png'
import reactRouterLogo from '../../images/react-router.png'
import materialUILogo from '../../images/material-ui.png'

const styles = {
  cardColDiv: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  cardRowDiv: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  card: {
    width: 600,
    marginBottom: '20px',
    marginRight: '10px',
    marginLeft: '10px',
  },
  reactMedia: {
    height: 150,
    backgroundSize: '212px 160px',
  },
  reduxMedia: {
    height: 150,
    backgroundSize: '125px 125px',
  },
  reactRouterMedia: {
    height: 150,
    backgroundSize: '144px 144px',
  },
  materialUIMedia: {
    height: 150,
    backgroundSize: '132px 105px',
  },
}

class Home extends Component {
  openLink = currentId => {
    console.log(currentId)
    // console.log(event.currentTarget);
    //console.log(event.currentTarget().id);
    // buttonId = event.currentTarget.id;
    let url
    switch (currentId) {
      case 'CreateReactApp':
        url = 'https://facebook.github.io/create-react-app/'
        break
      case 'Reactjs':
        url = 'https://reactjs.org'
        break
      case 'Redux':
        url = 'https://redux.js.org/'
        break
      case 'ReactRouter':
        url = 'https://reacttraining.com/react-router/'
        break
      case 'MaterialUI':
        url = 'https://material-ui.com/'
        break
      default:
        url = ``
    }
    window.open(url, '_blank')
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.cardColDiv}>
        <div className={classes.cardRowDiv}>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                className={classes.reactMedia}
                image={reactLogo}
                title="Open reactjs.org"
                onClick={() => this.openLink('Reactjs')}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  React
                </Typography>
                <Typography component="p">
                  React is a component base JavaScript library for building web
                  applications. Create React App provides the application
                  bootstrapping and webpack configuration.
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Tooltip title="Open Create React App">
                <Button
                  style={{ marginTop: 63 }}
                  size="small"
                  color="primary"
                  onClick={() => this.openLink('CreateReactApp')}>
                  Learn More
                </Button>
              </Tooltip>
            </CardActions>
          </Card>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                className={classes.reduxMedia}
                image={reduxLogo}
                title="Open reduxjs.org"
                onClick={() => this.openLink('Redux')}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Redux
                </Typography>
                <Typography component="p">
                  On it's own, React's only concept of state revolves around
                  component properties. This is where Redux comes in providing a
                  reliable state container. Redux is invaluable for connecting
                  sibling components when prop passing becomes awkward.
                  Additionally it provides a means to intelligently maintain the
                  data pulled into your application from external APIs.
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Tooltip title="Open Redux home page">
                <Button
                  size="small"
                  color="primary"
                  onClick={() => this.openLink('Redux')}>
                  Learn More
                </Button>
              </Tooltip>
            </CardActions>
          </Card>
        </div>
        <div className={classes.cardRowDiv}>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                className={classes.reactRouterMedia}
                image={reactRouterLogo}
                title="Open React Router home page"
                onClick={() => this.openLink('ReactRouter')}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  React Router
                </Typography>
                <Typography component="p">
                  React Router brings navigational components to the mix
                  transforming the React single page application into one that
                  is responsive to url paging.
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Tooltip title="Open React Router home page">
                <Button
                  size="small"
                  color="primary"
                  onClick={() => this.openLink('ReactRouter')}>
                  Learn More
                </Button>
              </Tooltip>
            </CardActions>
          </Card>
          <Card className={classes.card}>
            <CardActionArea>
              <CardMedia
                className={classes.materialUIMedia}
                image={materialUILogo}
                title="Open Material-UI home page"
                onClick={() => this.openLink('MaterialUI')}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Material-UI
                </Typography>
                <Typography component="p">
                  Material-UI is a React component library that implements
                  Google's Material Design specification.
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Tooltip title="Open Material-ui home page">
                <Button
                  size="small"
                  color="primary"
                  onClick={() => this.openLink('MaterialUI')}>
                  Learn More
                </Button>
              </Tooltip>
            </CardActions>
          </Card>
        </div>
      </div>
    )
  }
}

Home.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Home)
