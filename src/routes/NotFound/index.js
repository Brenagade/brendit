import React, { Component } from 'react'
import { history } from '../store'
import Button from '@material-ui/core/Button'

const styles = {
  container: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}

const goBack = e => {
  e.preventDefault()
  return history.goBack()
}

class NotFound extends Component {
  render() {
    return (
      <div style={styles.container}>
        <h4>Page Not Found</h4>
        <Button color="primary" onClick={goBack}>
          &larr; Back
        </Button>
      </div>
    )
  }
}

export default NotFound
