import React from 'react'
import { Switch, Route } from 'react-router-dom'
import About from './About'
import Home from './Home'
import NestedPartial from './NestedPartial'
import NotFound from './NotFound'
import SignUp from './SignUp'
import WebSocketExample from './WebSocketExample'
import SubReddIt from './SubReddIt'

export default () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/signup" component={SignUp} />
    <Route path="/nestedPartial" component={NestedPartial} />
    <Route path="/websocket" component={WebSocketExample} />
    <Route path="/about" component={About} />
    <Route path="/subReddIt" component={SubReddIt}/>
    <Route component={NotFound} />
  </Switch>
)
