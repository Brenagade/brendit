import React from 'react'
import PropTypes from 'prop-types'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import AccountCircleTwoToneIcon from '@material-ui/icons/AccountCircle'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import withStyles from '@material-ui/core/styles/withStyles'

const styles = theme => ({
  layout: {
    width: 'auto',
    display: 'block', // Fix IE11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
})

class SignUp extends React.Component {
  state = {
    name: '',
    password: '',
  }

  handleUserNameChange = event => {
    this.setState({ name: event.target.value })
  }

  handlePasswordChange = event => {
    this.setState({ password: event.target.value })
  }

  handleSignUp = () => {
    console.log(`user: ${this.state.name}, password: ${this.state.password}`)
  }

  render() {
    const { classes } = this.props
    return (
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <AccountCircleTwoToneIcon />
          </Avatar>
          <Typography variant="h4">Sign Up</Typography>
          <form className={classes.form}>
            {/*<FormControl margin="normal" required fullWidth>*/}
            {/*<InputLabel htmlFor="email">Email Address</InputLabel>*/}
            {/*<Input id="email" name="email" autoComplete="email" autoFocus/>*/}
            {/*</FormControl>*/}
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="user-name">User</InputLabel>
              <Input
                name="username"
                id="user-name"
                onChange={this.handleUserNameChange}
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input
                name="password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={this.handlePasswordChange}
              />
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.handleSignUp}>
              Sign Up
            </Button>
          </form>
        </Paper>
      </main>
    )
  }
}

SignUp.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(SignUp)
