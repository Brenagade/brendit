import { compose, createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction'
import createBrowserHistory from 'history/createBrowserHistory'
import thunk from 'redux-thunk'
import rootReducer, { getDefaultInitialState } from '../reducers'

// Initial Redux Store state is pulled from each reducer's initial state definition.
const initialState = getDefaultInitialState()

// Set up the router history off the base application name.
export const history = createBrowserHistory({ basename: '/brendit' })

// Build up the store
const store = createStore(
  rootReducer,
  initialState,
  compose(composeWithDevTools(applyMiddleware(thunk)))
)

if (module.hot) {
  module.hot.accept('../reducers', () => {
    const nextReducer = require('../reducers/index').default
    store.replaceReducer(nextReducer)
  })
}

export default store
