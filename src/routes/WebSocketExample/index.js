import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Snackbar from '@material-ui/core/Snackbar'
import TextField from '@material-ui/core/TextField'
import red from '@material-ui/core/colors/red'
import blue from '@material-ui/core/colors/blue'
import orange from '@material-ui/core/colors/orange'
import yellow from '@material-ui/core/colors/yellow'

const styles = theme => ({
  containerDiv: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  wsButton: {
    margin: theme.spacing.unit,
  },
  wsInputField: {
    floatingLabelText: {
      color: yellow,
    },
    floatingLabelStyle: {
      color: orange,
    },
    floatingLabelFocusStyle: {
      color: blue,
    },
    underlineStyle: {
      borderColor: red,
    },
  },
})

class WebSocketExample extends Component {
  constructor(props) {
    super(props)
    this.state = {
      snackbarOpen: false,
      inputDisabled: true,
      connectDisabled: false,
      disconnectDisabled: true,
      sendDisabled: true,
    }
    this.snackbarMsg = ''
    this.ws = null

    // HMR and fat arrow functions does not work. Alternative is to avoid them, but you have to bind this as needed.
    // https://github.com/gaearon/react-hot-loader/issues/221
    this.wsOpenCallback = this.wsOpenCallback.bind(this)
    this.wsConnect = this.wsConnect.bind(this)
    this.wsDisconnect = this.wsDisconnect.bind(this)
    this.wsMessageCallback = this.wsMessageCallback.bind(this)
    this.wsSendMessage = this.wsSendMessage.bind(this)
    this.handleInputTextChange = this.handleInputTextChange.bind(this)
    this.handleTextKeyPress = this.handleTextKeyPress.bind(this)
    this.handleSnackbarRequestClose = this.handleSnackbarRequestClose.bind(this)
  }

  wsOpenCallback(event) {
    console.log('WebSocket opened.', event)
    this.snackbarMsg = 'WebSocket is open.'
    this.setState({
      snackbarOpen: true,
      inputDisabled: false,
      connectDisabled: true,
      disconnectDisabled: false,
      sendDisabled: false,
      inputValue: '',
    })
  }

  wsConnect() {
    if (this.ws === null) {
      this.ws = new WebSocket(`ws://localhost:13001/messaging/echo`)
      console.log('this.ws', this.ws)
      this.ws.addEventListener('open', this.wsOpenCallback)
      this.ws.addEventListener('message', this.wsMessageCallback)
    } else {
      this.snackbarMsg = 'WebSocket is already connected'
      this.setState({ snackbarOpen: true })
    }
  }

  wsDisconnect() {
    this.ws.close()
    this.ws = null
    this.snackbarMsg = 'WebSocket Closed'
    this.setState({
      snackbarOpen: true,
      inputDisabled: true,
      connectDisabled: false,
      disconnectDisabled: true,
      sendDisabled: true,
    })
  }

  wsMessageCallback(event) {
    console.log('Message from server: ', event.data)
    this.snackbarMsg = event.data
    this.setState({ snackbarOpen: true })
  }

  wsSendMessage() {
    if (this.ws) {
      this.ws.send(this.state.inputValue)
    } else {
      this.snackbarMsg = 'WebSocket is not connected'
      this.setState({ snackbarOpen: true })
    }
  }

  handleInputTextChange(event) {
    this.setState({ inputValue: event.target.value })
  }

  handleTextKeyPress(event) {
    if (event.charCode === 13) {
      // enter key pressed
      event.preventDefault()
      this.wsSendMessage()
    }
  }

  handleSnackbarRequestClose() {
    this.setState({
      snackbarOpen: false,
    })
  }

  componentWillUnmount() {
    if (this.ws) {
      this.ws.close()
      this.ws = null
    }
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.containerDiv}>
        <h2 id="ws_main_heading">WebSocket - Echo</h2>
        <br />
        <br />
        <div>
          <Button
            className={classes.wsButton}
            id="ws_connect"
            disabled={this.state.connectDisabled}
            onClick={this.wsConnect}>
            Connect
          </Button>
          <Button
            className={classes.wsButton}
            id="ws_disconnect"
            disabled={this.state.disconnectDisabled}
            onClick={this.wsDisconnect}>
            Disconnect
          </Button>
          <Button
            className={classes.wsButton}
            id="ws_send"
            disabled={this.state.sendDisabled}
            onClick={this.wsSendMessage}>
            Send
          </Button>
        </div>
        <br />
        <TextField
          className={classes.wsInputField}
          id="ws_input_field"
          disabled={this.state.inputDisabled}
          onChange={this.handleInputTextChange}
          onKeyPress={this.handleTextKeyPress}
        />
        <Snackbar
          open={this.state.snackbarOpen}
          message={this.snackbarMsg}
          autoHideDuration={3000}
          onClose={this.handleSnackbarRequestClose}
        />
      </div>
    )
  }
}

WebSocketExample.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(WebSocketExample)
