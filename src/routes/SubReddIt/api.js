export function getNew() {
  return fetch('https://www.reddit.com/r/popular/hot.json ', {
    method: 'GET',
    redirect: 'follow',
    mode: 'cors',
    authority: 'www.reddit.com',
    headers: {
     'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
    },

  }).then(res => {
    if (res.ok) {
    //  window.location.href = res.redirect;
      return res.json()
    }
    return Promise.reject(Error(`error: ${res.status}, ${res.statusText}`))
  }).catch(error => {
    return Promise.reject(Error(error.message))
  })
}

export function getPopularNew() {
  return fetch('https://www.reddit.com/r/popular/new.json ', {
    method: 'GET',
    redirect: 'follow',
    mode: 'cors',
    authority: 'www.reddit.com',
    headers: {
      'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
    },

  }).then(res => {
    if (res.ok) {
      //  window.location.href = res.redirect;
      return res.json()
    }
    return Promise.reject(Error(`error: ${res.status}, ${res.statusText}`))
  }).catch(error => {
    return Promise.reject(Error(error.message))
  })
}

export function getPopularRising() {
  return fetch('https://www.reddit.com/r/popular/rising.json ', {
    method: 'GET',
    redirect: 'follow',
    mode: 'cors',
    authority: 'www.reddit.com',
    headers: {
      'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
    },

  }).then(res => {
    if (res.ok) {
      //  window.location.href = res.redirect;
      return res.json()
    }
    return Promise.reject(Error(`error: ${res.status}, ${res.statusText}`))
  }).catch(error => {
    return Promise.reject(Error(error.message))
  })
}

