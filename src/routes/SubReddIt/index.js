import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';

import { getNew } from './api'

import  Button  from '@material-ui/core/Button'
//import  Input  from '@material-ui/core/Input'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  button: {
    marginTop: theme.spacing.unit
  },
  gridRoot: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 1024,
    height: 700,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
})



class SubReddIt extends Component {
  state = {
    tileData: [],
    kind:"Redit",
    subreddit: '',
    view: '',
    labelWidth: 0,
  }
  componentDidMount(){
    let newValues = getNew()
    let tileData = []

    newValues.then(data => {
      const newdata = JSON.parse(JSON.stringify(data))

      let childrenData = newdata.data.children;
      let kind = newdata.kind;
      for (let i=0; i<newdata.data.dist; i++ ) {
        tileData.push({
          title: childrenData[i].data.title,
          url: childrenData[i].data.url,
          img: childrenData[i].data.thumbnail,
          author: childrenData[i].data.author,
          thumbnail_height: childrenData[i].data.thumbnail_height,
          thumbnail_width: childrenData[i].data.thumbnail_width
        })
      }
      this.setState({
        tileData: tileData,
        kind:kind,
        labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
      })

    }, error => {
      console.error('error getting', error);
    });

  }
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };


  handleNew = () => {
   let newValues = getNew()
   let tileData = []

   newValues.then(data => {
     const newdata = JSON.parse(JSON.stringify(data))

     let childrenData = newdata.data.children;
     for (let i=0; i<newdata.data.dist; i++ ) {
       tileData.push({
         title: childrenData[i].data.title,
         url: childrenData[i].data.url,
         img: childrenData[i].data.thumbnail,
         thumbnail_height: childrenData[i].data.thumbnail_height,
         thumbnail_width: childrenData[i].data.thumbnail_width
       })
     }

   }, error => {
     console.error('error getting', error);
   });
   this.setState({tileData: tileData})

 };

  render() {
    const { classes } = this.props;
    const {tileData, kind } = this.state;

    return (
      <div>
        <h2>SubReddIt</h2>
        <Button
          variant='contained'
          component='span'
          color='primary'
        onClick={this.handleNew}>Get New
        </Button>
        <form className={classes.root} autoComplete="off">
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel
            ref={ref => {
              this.InputLabelRef = ref;
            }}
            htmlFor="outlined-subreddit-simple">
            Subreddit
          </InputLabel>
          <Select
            value={this.state.subreddit}
            onChange={this.handleChange}
            input={
              <OutlinedInput
                labelWidth={this.state.labelWidth}
                name="subreddit"
                id="outlined-subreddit-simple"
              />
            }
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Popular</MenuItem>
            <MenuItem value={20}>gold</MenuItem>
            <MenuItem value={30}>News</MenuItem>
            <MenuItem value={40}>All</MenuItem>

          </Select>
        </FormControl>
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel
              ref={ref => {
                this.InputLabelRef = ref;
              }}
              htmlFor="outlined-view-simple">
              View
            </InputLabel>
            <Select
              value={this.state.view}
              onChange={this.handleChange}
              input={
                <OutlinedInput
                  labelWidth={this.state.labelWidth}
                  name="view"
                  id="outlined-view-simple"/>}>
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={10}>Hot</MenuItem>
              <MenuItem value={20}>New</MenuItem>
              <MenuItem value={30}>Controversial</MenuItem>
              <MenuItem value={40}>Top</MenuItem>
              <MenuItem value={50}>Rising</MenuItem>
            </Select>
          </FormControl>
        </form>
        <div className={classes.gridRoot}>
          <GridList cellHeight={180} className={classes.gridList} cols={3}>
            <GridListTile key="Subheader" cols={1} style={{ height: 'auto' }}>
              <ListSubheader component="div">{kind}</ListSubheader>
            </GridListTile>
            {tileData.map((tile, index) => (
              <GridListTile key={index}>
                <img src={tile.img} alt={tile.title} />
                <GridListTileBar
                  title={tile.title}
                  subtitle={<span>by: {tile.author}</span>}
                  actionIcon={
                    <IconButton
                      className={classes.icon}
                      href={tile.url}>
                      <InfoIcon />
                    </IconButton>
                  }
                />
              </GridListTile>
            ))}
          </GridList>
        </div>
      </div>
    )
  }
}

SubReddIt.propTypes = {
  classes: PropTypes.object.isRequired,
}
export default withStyles(styles)(SubReddIt)
