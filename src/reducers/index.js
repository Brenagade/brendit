import { combineReducers } from 'redux'

import PrincipalReducer, { getInitialPrincipalState } from './principal.reducer'
import TypedSnackbarReducer, {
  getInitialTypedSnackbarState,
} from './typed-snackbar.reducer'

const rootReducer = combineReducers({
  principal: PrincipalReducer,
  typedSnackbar: TypedSnackbarReducer,
})

export const getDefaultInitialState = () => {
  const initialState = {}

  initialState.principal = getInitialPrincipalState()
  initialState.typedSnackbar = getInitialTypedSnackbarState()

  return initialState
}

export default rootReducer
