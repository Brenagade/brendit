import { principalActionTypes } from '../actions/principal.actions'

const initialState = {
  signInOpen: false,
  credentials: {},
  apiKey: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case principalActionTypes.SIGN_IN_SHOW:
      return {
        ...state,
        signInOpen: true,
      }
    case principalActionTypes.SIGN_IN_HIDE:
      return {
        ...state,
        signInOpen: false,
      }
    case principalActionTypes.SET_CREDENTIALS:
      return {
        ...state,
        credentials: action.payload,
      }
    case principalActionTypes.SET_API_KEY:
      return {
        ...state,
        apiKey: action.payload,
      }
    default:
      return state
  }
}

export const getInitialPrincipalState = () => {
  return initialState
}
