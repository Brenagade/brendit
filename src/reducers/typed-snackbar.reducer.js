import { typedSnackbarActionTypes } from '../actions/typed-snackbar.actions'

const intialState = {
  message: '',
  open: false,
  variant: '',
}

export default (state = intialState, action) => {
  switch (action.type) {
    case typedSnackbarActionTypes.TYPED_SNACKBAR_ADD_MESSAGE:
      return {
        ...state,
        message: action.message,
        open: true,
        variant: action.variant,
      }
    case typedSnackbarActionTypes.TYPED_SNACKBAR_GET_MESSAGE:
      return state
    case typedSnackbarActionTypes.TYPED_SNACKBAR_CLOSE_MESSAGE:
      return {
        ...state,
        open: false,
      }
    default:
      return state
  }
}

export const getInitialTypedSnackbarState = () => {
  return intialState
}
