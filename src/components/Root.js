import React, { Component } from 'react'
import { Route, Router } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'

import App from './containers/AppContainer'

/*
 * The application top level route.
 */
class Root extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <Router basename="/brendit" history={this.props.history}>
          <Route path="/" component={App} />
        </Router>
      </Provider>
    )
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
}

export default Root
