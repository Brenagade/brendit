import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
//
// Components
//
import AppBar from '@material-ui/core/AppBar'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import SignIn from './containers/SignInContainer'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import TypedSnackbar from './containers/TypedSnackbarContainer'
//
// Icons
//
import AccountCircle from '@material-ui/icons/AccountCircle'
import BookIcon from '@material-ui/icons/Book'
import HomeIcon from '@material-ui/icons/Home'
import MenuIcon from '@material-ui/icons/Menu'
import PermIdentityTwoToneIcon from '@material-ui/icons/PermIdentityTwoTone'
import PermMediaIcon from '@material-ui/icons/PermMedia'
import ImportExportIcon from '@material-ui/icons/ImportExport'
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'

import logo from '../images/react.svg'

import { withStyles } from '@material-ui/core/styles'
import withRoot from './withRoot'

import Routes from '../routes/routes'

const drawerWidth = 240
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: 8,
    marginRight: 20,
  },
  appFrame: {
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBar: {
    position: 'fixed',
    top: 0,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'appBarShift-left': {
    marginLeft: drawerWidth,
  },
  'appBarShift-right': {
    marginRight: drawerWidth,
  },
  hide: {
    display: 'none',
  },
  appBarImg: {
    height: '60px',
    width: '60px',
    marginRight: '25px',
  },
  appRightToolbar: {
    marginLeft: 'auto',
    marginRight: 8,
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
    backgroundColor: '#fafafa',
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  'content-left': {
    marginLeft: -drawerWidth,
  },
  'content-right': {
    marginRight: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'contentShift-left': {
    marginLeft: 0,
  },
  'contentShift-right': {
    marginRight: 0,
  },
})

class App extends React.Component {
  state = {
    auth: true,
    anchorEl: null,
    mobileOpen: false,
    open: false,
    anchor: 'left',
  }

  handleDrawerOpen = () => {
    this.setState({ open: true })
  }

  handleDrawerClose = () => {
    this.setState({ open: false })
  }

  handleChangeAnchor = event => {
    this.setState({
      anchor: event.target.value,
    })
  }

  handleAuthMenu = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleAuthClose = () => {
    this.setState({ anchorEl: null })
  }

  handleSignIn = () => {
    this.handleAuthClose()
    this.props.showSignInForm()
  }

  render() {
    const { classes, theme, signInOpen } = this.props
    const { auth, anchor, open, anchorEl } = this.state
    const authOpen = Boolean(anchorEl)
    const drawer = (
      <Drawer
        variant="persistent"
        anchor={anchor}
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}>
        <div className={classes.drawerHeader}>
          <IconButton onClick={this.handleDrawerClose}>
            {theme.direction === 'rtl' ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <List>
          {' '}
          <div>
            <Link to="/signup">
              <ListItem button onClick={this.handleDrawerClose}>
                <ListItemIcon>
                  <PermIdentityTwoToneIcon color="primary" />
                </ListItemIcon>
                <ListItemText primary="Sign Up" />
              </ListItem>
            </Link>
            <Link to="/">
              <ListItem button onClick={this.handleDrawerClose}>
                <ListItemIcon>
                  <HomeIcon color="primary" />
                </ListItemIcon>
                <ListItemText primary="Home" />
              </ListItem>
            </Link>
            <Link to="/subReddIt">
              <ListItem button onClick={this.handleDrawerClose}>
                <ListItemIcon>
                  <BookIcon color="primary" />
                </ListItemIcon>
                <ListItemText primary="SubReddIt" />
              </ListItem>
            </Link>

            <Link to="/nestedPartial">
              <ListItem button onClick={this.handleDrawerClose}>
                <ListItemIcon>
                  <PermMediaIcon color="secondary" />
                </ListItemIcon>
                <ListItemText primary="Nested" />
              </ListItem>
            </Link>
            <Link to="/websocket">
              <ListItem button onClick={this.handleDrawerClose}>
                <ListItemIcon>
                  <ImportExportIcon color="error" />
                </ListItemIcon>
                <ListItemText primary="WebSocket Example" />
              </ListItem>
            </Link>
            <Link to="/about">
              <ListItem button onClick={this.handleDrawerClose}>
                <ListItemIcon>
                  <QuestionAnswerIcon color="disabled" />
                </ListItemIcon>
                <ListItemText primary="About" />
              </ListItem>
            </Link>
          </div>
        </List>
      </Drawer>
    )

    return (
      <div className={classes.root}>
        <div className={classes.appFrame}>
          <AppBar
            className={classNames(classes.appBar, {
              [classes.appBarShift]: open,
              [classes[`appBarShift-${anchor}`]]: open,
            })}>
            <Toolbar disableGutters={!open}>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={this.handleDrawerOpen}
                className={classNames(
                  classes.menuButton,
                  open && classes.hide
                )}>
                <MenuIcon />
              </IconButton>
              <img className={classes.appBarImg} src={logo} alt="logo" />
              <Typography variant="h4" color="inherit" noWrap>
                Brenda's Assignment
              </Typography>
              {auth && (
                <div className={classes.appRightToolbar}>
                  <IconButton
                    aria-owns={authOpen ? 'menu-appbar' : null}
                    aria-haspopup="true"
                    onClick={this.handleAuthMenu}
                    color="inherit">
                    <AccountCircle />
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={authOpen}
                    onClose={this.handleAuthClose}>
                    <MenuItem onClick={this.handleSignIn}>Sign In</MenuItem>
                  </Menu>
                </div>
              )}
            </Toolbar>
          </AppBar>
          {drawer}
          <main
            className={classNames(
              classes.content,
              classes[`content-${anchor}`],
              {
                [classes.contentShift]: open,
                [classes[`contentShift-${anchor}`]]: open,
              }
            )}>
            <div className={classes.drawerHeader} />
            {signInOpen ? <SignIn /> : <Routes />}
          </main>
        </div>
        <TypedSnackbar />
      </div>
    )
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  signInOpen: PropTypes.bool,
  showSignInForm: PropTypes.func,
}

export default withRoot(withStyles(styles, { withTheme: true })(App))
