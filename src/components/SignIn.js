import React from 'react'
import PropTypes from 'prop-types'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import LockIcon from '@material-ui/icons/LockOutlined'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import withStyles from '@material-ui/core/styles/withStyles'

const styles = theme => ({
  layout: {
    width: 'auto',
    display: 'block', // Fix IE11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
})

class SignIn extends React.Component {
  state = {
    name: '',
    password: '',
    apiKey: '',
  }

  handleUserNameChange = event => {
    this.setState({ name: event.target.value })
  }

  handlePasswordChange = event => {
    this.setState({ password: event.target.value })
  }

  handleAPIKeyChange = event => {
    this.setState({ apiKey: event.target.value })
  }

  handleSignIn = () => {
    console.log(
      `user: ${this.state.name}, password: ${this.state.password}, server: ${
        this.state.apiKey
      }`
    )
    const credentials = {
      name: this.state.name,
      pw: this.state.password,
    }
    this.props.setCredentials(credentials)
    this.props.setAPIKey(this.state.apiKey)
    this.props.hideSignInForm()
  }

  render() {
    const { classes, signInOpen } = this.props

    if (signInOpen) {
      return (
        <React.Fragment>
          <CssBaseline />
          <main className={classes.layout}>
            <Paper className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockIcon />
              </Avatar>
              <Typography variant="h4">Sign in</Typography>
              <form className={classes.form}>
                {/*<FormControl margin="normal" required fullWidth>*/}
                {/*<InputLabel htmlFor="email">Email Address</InputLabel>*/}
                {/*<Input id="email" name="email" autoComplete="email" autoFocus/>*/}
                {/*</FormControl>*/}
                <FormControl margin="normal" required fullWidth>
                  <InputLabel htmlFor="user-name">User</InputLabel>
                  <Input
                    name="username"
                    id="user-name"
                    onChange={this.handleUserNameChange}
                  />
                </FormControl>
                <FormControl margin="normal" required fullWidth>
                  <InputLabel htmlFor="password">Password</InputLabel>
                  <Input
                    name="password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onChange={this.handlePasswordChange}
                  />
                </FormControl>
                <FormControl margin="normal" required fullWidth>
                  <InputLabel htmlFor="apiKey">Artifactory API Key</InputLabel>
                  <Input
                    name="apiKey"
                    id="apiKey"
                    value={this.state.apiKey}
                    onChange={this.handleAPIKeyChange}
                  />
                </FormControl>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  onClick={this.handleSignIn}>
                  Sign in
                </Button>
              </form>
            </Paper>
          </main>
        </React.Fragment>
      )
    } else {
      return <div />
    }
  }
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired,
  signInOpen: PropTypes.bool,
  hideSignInForm: PropTypes.func,
  setCredentials: PropTypes.func,
  setAPIKey: PropTypes.func,
}

export default withStyles(styles)(SignIn)
