import React from 'react'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
// import orange from '@material-ui/core/colors/orange'
// import green from '@material-ui/core/colors/green'
import CssBaseline from '@material-ui/core/CssBaseline'

// A theme with custom primary and secondary color.
// It's optional.
const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {
      light: '#72A039',
      main: '#0e3f48',
      dark: '#0d2029',
    },
    secondary: {
      light: '#0e9747',
      main: '#076938',
      dark: '#0e3b44',
    },
  },
})

function withRoot(Component) {
  function WithRoot(props) {
    // MuiThemeProvider makes the theme available down the React tree
    // thanks to React context.
    return (
      <MuiThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Component {...props} />
      </MuiThemeProvider>
    )
  }

  return WithRoot
}

export default withRoot
