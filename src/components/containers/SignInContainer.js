import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import SignIn from '../SignIn'
import {
  hideSignInForm,
  setCredentials,
  setAPIKey,
} from '../../actions/principal.actions'

class SignInContainer extends Component {
  render() {
    return (
      <SignIn
        signInOpen={this.props.signInOpen}
        hideSignInForm={this.props.hideSignInForm}
        setCredentials={this.props.setCredentials}
        setAPIKey={this.props.setAPIKey}
      />
    )
  }
}

SignInContainer.propTypes = {
  signInOpen: PropTypes.bool,
  hideSignInForm: PropTypes.func,
  setCredentials: PropTypes.func,
  setAPIKey: PropTypes.func,
}

const mapStateToProps = state => {
  return {
    signInOpen: state.principal.signInOpen,
    credentials: state.principal.credentials,
    apiKey: state.principal.apiKey,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    hideSignInForm: () => {
      dispatch(hideSignInForm())
    },
    setCredentials: credentials => {
      dispatch(setCredentials(credentials))
    },
    setAPIKey: key => {
      dispatch(setAPIKey(key))
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInContainer)
