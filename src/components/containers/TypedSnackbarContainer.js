import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import TypedSnackbar from '../TypedSnackbar'
import * as actions from '../../actions/typed-snackbar.actions'

class TypedSnackbarContainer extends Component {
  render() {
    return (
      <TypedSnackbar
        typedSnackbar={this.props.typedSnackbar}
        closeSnackbar={this.props.typedSnackbarActions.closeSnackbar}
      />
    )
  }
}

TypedSnackbarContainer.propTypes = {
  typedSnackbar: PropTypes.object,
}

const mapStateToProps = state => {
  return {
    typedSnackbar: state.typedSnackbar,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    typedSnackbarActions: {
      success: msg => {
        dispatch(actions.successSnackbar(msg))
      },
      info: msg => {
        dispatch(actions.infoSnackbar(msg))
      },
      warn: msg => {
        dispatch(actions.warningSnackbar(msg))
      },
      error: msg => {
        dispatch(actions.errorSnackbar(msg))
      },
      closeSnackbar: () => {
        dispatch(actions.closeMessage())
      },
    },
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TypedSnackbarContainer)
)
