import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import App from '../App'
import { showSignInForm } from '../../actions/principal.actions'

class AppContainer extends Component {
  render() {
    return (
      <App
        signInOpen={this.props.signInOpen}
        showSignInForm={this.props.showSignInForm}
      />
    )
  }
}

AppContainer.propTypes = {
  signInOpen: PropTypes.bool,
  showSignInForm: PropTypes.func,
}

const mapStateToProps = state => {
  return {
    signInOpen: state.principal.signInOpen,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showSignInForm: () => {
      dispatch(showSignInForm())
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppContainer)
