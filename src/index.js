import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { AppContainer } from 'react-hot-loader'
import * as serviceWorker from './serviceWorker'
import 'typeface-roboto'

import store, { history } from './routes/store'
import Root from './components/Root'

function renderApp(RootComponent) {
  ReactDOM.render(
    <AppContainer>
      <RootComponent store={store} history={history} />
    </AppContainer>,
    document.getElementById('root')
  )
  // If you want your app to work offline and load faster, you can change
  // unregister() to register() below. Note this comes with some pitfalls.
  // Learn more about service workers: http://bit.ly/CRA-PWA
  serviceWorker.unregister()
}

renderApp(Root)

if (module.hot) {
  module.hot.accept('./components/Root', () => {
    const NextApp = require('./components/Root').default
    renderApp(NextApp)
  })
}
