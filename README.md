# brendit

This project was created wit a React Web App boilerplate. It was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
The most recent version of the guide is [here](https://facebook.github.io/create-react-app/docs/getting-started).

Quick note. There is a warning in the browser console. React-Hot-Loader: react-fire-dom 
patch is not detected. React 16.6+ features may not work. It can be safely ignored as
React fire is not out yet.

See [issue](https://github.com/gatsbyjs/gatsby/issues/11934) which is tracking the problem.

## Table of Contents

- [Quick Start](#quick-start)
- [Assignment](#take-home-assignment-details)

## Quick Start

### Requirements
* [yarn](https://yarnpkg.com/en/)
* [node](https://nodejs.org/en/)
* [nodemon](https://nodemon.io/) - optional, replace `nodemon` with `node` in `package.json` to skip server monitoring
* [docker](https://www.docker.com/) - optional

### Install dependencies:
```
yarn install
```

### Run local development:
This runs the application locally in a Webpack development server. The hot module replacement functions in all 
development configurations, local and docker container.
```
yarn start
```
run with https://localhost:13000/brendit

### WebSocket Server
In a new terminal, at the project root, start the WebSocket server.
```
yarn run start:wss
```

### Run production:
TBD - will a local production configuration be supported?
```
yarn build
yarn run start:prod
```
run with http://localhost:13000/brendit

### Using Docker Compose
Requires [Docker Compose](https://docs.docker.com/compose/)

Using the Docker support for brendit starts two containers. One production and
one for development. The production versions exposes URLs on two ports (443 and 8090).
SSL/TLS on https://localhost/brendit and unsecured on http://localhost:8090/brendit.
The development access on http://localhost:13000/brendit has React Hot Module replacement
as well as change detection on the Express server thanks to nodemon. WebSocket uses
port 13001.

To start:
```
docker-compose up
```

To force build and start:
```
yarn build
docker-compose up --build
```

Check the status of the running containers:
```
docker ps
CONTAINER ID        IMAGE               COMMAND                CREATED             STATUS              PORTS                                                  NAMES
39a4db4cdfee        brendit_wss            "npm run start:wss"    32 minutes ago      Up 32 minutes       0.0.0.0:13001->13001/tcp                               brendit_wss_1
e871ff9a1ada        brendit_dev            "npm run start"        32 minutes ago      Up 32 minutes       0.0.0.0:13000->13000/tcp                               brendit_dev_1
7bc5341de1ec        brendit_web            "/bin/sh -c 'nginx'"   About an hour ago   Up 32 minutes       0.0.0.0:443->443/tcp, 80/tcp, 0.0.0.0:8090->8090/tcp   brendit_web_1
```
To stop docker:
```$xslt
docker-compose down
```

The following URLs are available on the browser:

1.  https://localhost/brendit
1.  http://localhost:8090/brendit
1.  http://localhost:13000/brendit - debug version with full hot module replacement

If the browser complains:
_Error during service worker registration: DOMException: Failed to register a ServiceWorker: An SSL certificate error occurred when fetching the script._

Start it with this command:
```
/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --user-data-dir=/tmp/foo --ignore-certificate-errors --unsafely-treat-insecure-origin-as-secure=https://localhost/brendit
```


Stop containers:
```
docker-compose stop
```

Remove containers:
```
docker container rm brendit_dev_1 brendit_web_1 brendit_wss_1 brendit_db_1
```

Remove images:
```
docker image rm brendit_dev brendit_web brendit_wss brendit_db
```
## Technology Stack

### Web Client

* [Reactjs](https://facebook.github.io/react/)
* [Redux](http://redux.js.org/)
* [React Router](https://reacttraining.com/react-router/)
* [Material UI](http://www.material-ui.com/#/)

### Web Server

* [Node.js](https://nodejs.org/en/)
* [Express](https://expressjs.com/)
* [WebSocket](https://github.com/websockets/ws)

### Tools, Development, and Test

* [Babel](https://babeljs.io/)
* [Chai](http://chaijs.com/)
* [Mocha](https://mochajs.org/)
* [Enzyme](http://airbnb.io/enzyme/index.html)
* [Eslint](https://eslint.org/)
* [JSDOM](https://github.com/tmpvar/jsdom)
* [React Hot Loader](http://gaearon.github.io/react-hot-loader/)
* [nodemon](https://nodemon.io/)
* [fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)

### Build and Bundling

* [webpack](https://webpack.js.org/)

### Deployment

* [nginx](https://www.nginx.com/resources/wiki/community/)
* [Docker](https://www.docker.com/community-edition)

#brendIt
##Decisions
##Program Information

#Take-Home Assignment Details 

###Reddit-Lite
Using the reddit API ​https://www.reddit.com/dev/api/​, recreate reddit! More specifically, create a subreddit viewer 
that displays a list of posts in any subreddit, 25 at a time with the ability to page for more.
* Each post should include the title, a link to the post/site, a thumbnail if it exists, the user who posted it, 
time submitted, and a link to the comments on reddit.
* The viewer should automatically refresh the data every minute (ideally, without losing position of the page so that 
the content doesn’t jump around).
* Do not spend too much time worrying about aesthetics; the site should follow basic design patterns, but stick to 
using default colour schemes/control skins/etc.

* An example subreddit with very little custom CSS to compare with is
https://www.reddit.com/r/news
* Do not use any Reddit JS libraries for API access.
* Hint: no special API token or account is needed. Reddit allows a .json extension (or
simply fetching with appropriate content headers) and responds with a JSON payload on a URL schema of
 ​https://reddit.com/r/{subreddit}/{view}
Deliverables
Along with a working app and appropriate tests (unit, integration, etc), please include a brief explanation of code 
structure. As mentioned above, don’t forget, “it doesn’t exist if it’s not written down”.
Technology
Please use React and Redux. The rest of the technology choices are up to you.